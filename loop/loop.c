#include "gen.h"

static inline int port_init(uint8_t port, struct gen_config *config) {
	struct rte_eth_conf port_conf = port_conf_default;
	const uint16_t rx_rings = 1, tx_rings = 1;
	int retval;
	uint16_t q;

    char name[7];

    snprintf(name, sizeof(name), "RX%02u:%02u", port, (unsigned)0);
    printf("%s\n", name);
    config->rx_pool = rte_pktmbuf_pool_create(name, RX_RING_SIZE,
            0, 0, RTE_MBUF_DEFAULT_BUF_SIZE, rte_socket_id());

	if (config->rx_pool == NULL) {
		rte_exit(EXIT_FAILURE, "Cannot create RX mbuf pool: %s\n", rte_strerror(rte_errno));
    }

    snprintf(name, sizeof(name), "%s%02u:%02u", "TX", port, (unsigned)0);
    config->tx_pool = rte_pktmbuf_pool_create(name, NUM_PKTS,
            0, 0, RTE_MBUF_DEFAULT_BUF_SIZE, rte_socket_id());

	if (config->tx_pool == NULL) {
		rte_exit(EXIT_FAILURE, "Cannot create TX mbuf pool: %s\n", rte_strerror(rte_errno));
    }

	if (port >= rte_eth_dev_count())
		return -1;

	/* Configure the Ethernet device. */
	retval = rte_eth_dev_configure(port, rx_rings, tx_rings, &port_conf);
	if (retval != 0) {
		return retval;
    }

	/* Allocate and set up 1 RX queue per Ethernet port. */
	for (q = 0; q < rx_rings; q++) {
		retval = rte_eth_rx_queue_setup(port, q, RX_RING_SIZE,
				rte_eth_dev_socket_id(port), NULL, config->rx_pool);
		if (retval < 0) {
			return retval;
        }
#if MEASURE_LATENCY
        void *p = rte_eth_add_rx_callback(port, q, gen_stamp_pkts_rx, NULL);
        if (p == NULL) {
            return -1;
        }
#endif
	}

	/* Allocate and set up 1 TX queue per Ethernet port. */
	for (q = 0; q < tx_rings; q++) {
		retval = rte_eth_tx_queue_setup(port, q, TX_RING_SIZE,
				rte_eth_dev_socket_id(port), NULL);
		if (retval < 0) {
			return retval;
        }
#if MEASURE_LATENCY
        void *p = rte_eth_add_tx_callback(port, q, gen_stamp_pkts_tx, NULL);
        if (p == NULL) {
            return -1;
        }
#endif
	}

	/* Start the Ethernet port. */
	retval = rte_eth_dev_start(port);
	if (retval < 0) {
		return retval;
    }

	/* Display the port MAC address. */
	struct ether_addr addr;
	rte_eth_macaddr_get(port, &addr);
	printf("Port %u MAC: %02" PRIx8 " %02" PRIx8 " %02" PRIx8
			   " %02" PRIx8 " %02" PRIx8 " %02" PRIx8 "\n",
			(unsigned)port,
			addr.addr_bytes[0], addr.addr_bytes[1],
			addr.addr_bytes[2], addr.addr_bytes[3],
			addr.addr_bytes[4], addr.addr_bytes[5]);

	/* Enable RX in promiscuous mode for the Ethernet device. */
	rte_eth_promiscuous_enable(port);

	return 0;
}

static int lcore_init(void *arg) {
    struct gen_config *config = (struct gen_config*)arg;
    printf("Init core %d\n", rte_lcore_id());

    config->seed.a = 1;
    config->seed.b = 2;
    config->seed.c = 3;
    config->seed.d = 4;
    raninit(&config->seed, (u8) get_time_sec());

    if (port_init(config->port, config) != 0) {
        rte_exit(EXIT_FAILURE, "Cannot init port %"PRIu8 "\n", config->port);
    }

    return 0;
}

static void stats(uint32_t *tx_rate UNUSED, uint64_t *tx_bytes, uint64_t *tx_pkts,
    double *start_time, uint64_t *cnt UNUSED, uint64_t *rx_bytes, uint64_t *rx_pkts) {
    double now = get_time_sec();
    double elapsed = now - *start_time;
    if (elapsed > 1.0f) {
        double tx_bps = (8 * *tx_bytes)/elapsed;
        double tx_pps = *tx_pkts/elapsed;
        double rx_bps = (8 * *rx_bytes)/elapsed;
        double rx_pps = *rx_pkts/elapsed;
        printf("Core %u: tx_pps: %.0f tx_gbps: %.2f rx_pps: %.0f rx_gbps: %.2f\n",
               rte_lcore_id(), tx_pps, tx_bps/1000000000.0f,
               rx_pps, rx_bps / 1000000000.0f);
        *rx_pkts = 0;
        *rx_bytes = 0;
        *start_time = now;
        *tx_bytes = 0;
        *tx_pkts = 0;
        *cnt = 0;
    }
}

static void main_loop(struct gen_config *config) {
    double start_time = get_time_sec();
    uint8_t i;
    uint32_t nb_tx, tx_rate = config->tx_rate;
    uint64_t tx_bytes = 0, tx_pkts = 0, cnt = 0;

    struct gen_config cfg;
    cfg.port = config->port;
    cfg.role = config->role;
    cfg.tx_rate = config->tx_rate;
    cfg.warmup = config->warmup;
    cfg.duration = config->duration;
    cfg.num_flows = config->num_flows;
    cfg.ip_min = config->ip_min;
    cfg.udp_min = config->udp_min;
    cfg.size_min = config->size_min;
    cfg.size_max = config->size_max;
    cfg.seed = config->seed;
    cfg.rx_pool = config->rx_pool;
    cfg.tx_pool = config->tx_pool;

    struct rte_mbuf *rx_bufs[RX_RING_SIZE];
    uint32_t nb_rx;
    uint64_t rx_bytes = 0, rx_pkts = 0;

	printf("\nCore %u running.\n", rte_lcore_id());

    for (;;) {
        stats(&tx_rate, &tx_bytes, &tx_pkts, &start_time, &cnt, &rx_bytes, &rx_pkts);
        nb_rx = rte_eth_rx_burst(cfg.port, 0, rx_bufs, RX_RING_SIZE);

        for (i = 0; i < nb_rx; i++) {
            rx_bytes += rx_bufs[i]->pkt_len;
        }

        rx_pkts += nb_rx;

        nb_tx = rte_eth_tx_burst(cfg.port, 0, rx_bufs, nb_rx);
        while (unlikely(nb_tx < nb_rx)) {
            nb_tx += rte_eth_tx_burst(cfg.port, 0, rx_bufs + nb_tx, nb_rx - nb_tx);
        }

        for (i = 0; i < nb_rx; i++) {
            rte_pktmbuf_free(rx_bufs[i]);
        }

        tx_pkts += nb_tx;
        tx_bytes = rx_bytes;
    }
}

static int launch_lcore(void *config) {
    main_loop((struct gen_config*)config);
    return 0;
}

int main(int argc, char *argv[]) {
    uint8_t nb_ports, port, nb_cores, core;

	/* Initialize EAL */
	int ret = rte_eal_init(argc, argv);
	if (ret < 0) {
		rte_exit(EXIT_FAILURE, "Error with EAL initialization\n");
    }

	nb_ports = rte_eth_dev_count();
    nb_cores = rte_lcore_count();

    /* Initialize ports */
    uint8_t port_map[nb_cores];
    core = 0;
    for (port = 0; port < nb_ports; port++) {
        port_map[core] = port;
        core++;
    }

    /* Launch generator */
    int i;
    core = 0;
    port = 0;
    struct gen_config config[nb_cores];
    RTE_LCORE_FOREACH_SLAVE(i) {
        if (port == nb_ports) {
            break;
        }
        config[i].port = port_map[core];
        core++;
        port++;

        rte_eal_remote_launch(lcore_init, (void*)&config[i], i);
        rte_eal_wait_lcore(i);
        rte_eal_remote_launch(launch_lcore, (void*)&config[i], i);
    }
    rte_eal_mp_wait_lcore();

	return 0;
}

#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>
#include <ctype.h>
#include <getopt.h>
#include <rte_eal.h>
#include <rte_random.h>
#include <rte_errno.h>
#include <rte_ethdev.h>
#include <rte_cycles.h>
#include <rte_lcore.h>
#include <rte_launch.h>
#include <rte_mbuf.h>
#include <rte_common.h>
#include <rte_ether.h>
#include <rte_ip.h>
#include <rte_udp.h>
#include <rte_memcpy.h>

#define NUM_PKTS (1<<16)
#define BURST_SIZE 32

#define UNUSED __attribute__((__unused__))

#define MBUF_CACHE_SIZE 512

#define MEASURE_LATENCY 0

#if MEASURE_LATENCY

#define RX_RING_SIZE 128
#define TX_RING_SIZE 64

#else

#define RX_RING_SIZE 1024
#define TX_RING_SIZE 1024

#endif

/* smallprng
 * source: http://burtleburtle.net/bob/rand/smallprng.html
 */
typedef unsigned long long u8;
typedef struct ranctx { u8 a; u8 b; u8 c; u8 d; } ranctx;

#define rot(x,k) (((x)<<(k))|((x)>>(64-(k))))
static u8 ranval( ranctx *x ) {
    u8 e = x->a - rot(x->b, 7);
    x->a = x->b ^ rot(x->c, 13);
    x->b = x->c + rot(x->d, 37);
    x->c = x->d + e;
    x->d = e + x->a;
    return x->d;
}

static void raninit( ranctx *x, u8 seed ) {
    u8 i;
    x->a = 0xf1ea5eed, x->b = x->c = x->d = seed;
    for (i=0; i<20; ++i) {
        (void)ranval(x);
    }
}

enum{ROLE_CONSUMER = 0, ROLE_PRODUCER};

struct gen_config {
    uint8_t port;
    uint8_t role;

    uint32_t tx_rate;
    uint32_t warmup;
    uint32_t duration;

    uint32_t num_flows;
    uint32_t ip_src;
    uint32_t ip_min;
    uint16_t udp_min;

    uint16_t size_min;
    uint16_t size_max;

    double start_time;

    struct rte_mempool *tx_pool;
    struct rte_mempool *rx_pool;

    ranctx seed;
};

static const struct rte_eth_conf port_conf_default = {
	.rxmode = { .max_rx_pkt_len = ETHER_MAX_LEN }
};

static struct ether_addr ether_src UNUSED =
    {{ 0x00, 0x01, 0x02, 0x03, 0x04, 0x00 }};

static struct ether_addr ether_dst UNUSED =
    {{ 0x00, 0x01, 0x02, 0x03, 0x04, 0x01 }};

double get_time_sec(void);

uint16_t gen_pkt_size(struct gen_config *config);

double get_time_sec(void) {
    return rte_get_tsc_cycles() / (double) rte_get_tsc_hz();
}

uint16_t gen_pkt_size(struct gen_config *config) {
    return (uint16_t) ranval(&config->seed) % (RTE_MAX(config->size_max - config->size_min, 1)) +
        config->size_min;
}

#if MEASURE_LATENCY
static uint16_t gen_stamp_pkts_tx(uint8_t port UNUSED, uint16_t q UNUSED,
        struct rte_mbuf *bufs[], uint16_t n, void* arg UNUSED) {
    double now = get_time_sec();
    int i;
    for (i = 0; i < n; i++) {
        double *p = rte_pktmbuf_mtod_offset(bufs[i], double *,
                    sizeof(struct ether_hdr) + sizeof(struct ipv4_hdr) +
                    sizeof(struct udp_hdr));
        *p = now;
    }
    return n;
}

static uint16_t gen_stamp_pkts_rx(uint8_t port UNUSED, uint16_t q UNUSED,
        struct rte_mbuf *bufs[], uint16_t n, uint16_t m UNUSED, void* arg UNUSED) {
    double now = get_time_sec();
    int i;
    for (i = 0; i < n; i++) {
        double *p = rte_pktmbuf_mtod_offset(bufs[i], double *,
                    sizeof(struct ether_hdr) + sizeof(struct ipv4_hdr) +
                    sizeof(struct udp_hdr));
        *p = now - *p;
    }
    return n;
}
#endif

/* Consumer */
void consumer_loop(struct gen_config *config);

int launch_consumer(void *config);

/* Producer */
void producer_loop(struct gen_config *config);

int launch_producer(void *config);

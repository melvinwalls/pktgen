static void stats(uint32_t *tx_rate UNUSED, uint64_t *tx_bytes, uint64_t *tx_pkts,
    double *start_time, uint64_t *cnt UNUSED, uint64_t *rx_bytes, uint64_t *rx_pkts, double *delay,
    double *mrxpps, double *varrxpps, double *mrxbps, double *varrxbps, double *mrxwire, double *varrxwire,
    double *mtxpps, double *vartxpps, double *mtxbps, double *vartxbps, double *mtxwire, double *vartxwire,
    uint64_t *n) {
    double now = get_time_sec();
    double elapsed = now - *start_time;
    double tx_bps = (8 * *tx_bytes)/elapsed;
    double tx_pps = *tx_pkts/elapsed;
    double rx_bps = (8 * *rx_bytes)/elapsed;
    double rx_pps = *rx_pkts/elapsed;

    *n = *n + 1;
    /* update tx bps mean/var */
    double delta = tx_bps / 1000000 - *mtxbps;
    *mtxbps += delta / *n;
    *vartxbps += delta * (tx_bps / 1000000 - *mtxbps);

    /* update tx pps mean/var */
    delta = tx_pps / 1000000 - *mtxpps;
    *mtxpps += delta / *n;
    *vartxpps += delta * (tx_pps / 1000000 - *mtxpps);

    /* update tx wire rate mean/var */
    double wire = ((tx_bps / 1000000) + ((tx_pps / 1000000) * 20 * 8));
    delta = wire - *mtxwire;
    *mtxwire += delta / *n;
    *vartxwire += delta * (wire - *mtxwire);

    /* update rx bps mean/var */
    delta = rx_bps / 1000000 - *mrxbps;
    *mrxbps += delta / *n;
    *varrxbps += delta * (rx_bps / 1000000 - *mrxbps);

    /* update rx pps mean/var */
    delta = rx_pps / 1000000 - *mrxpps;
    *mrxpps += delta / *n;
    *varrxpps += delta * (rx_pps / 1000000 - *mrxpps);

    /* update rx wire rate mean/var */
    wire = ((rx_bps / 1000000) + ((rx_pps / 1000000) * 20 * 8));
    delta = wire - *mrxwire;
    *mrxwire += delta / *n;
    *varrxwire += delta * (wire - *mrxwire);

    if (elapsed >= 1.0f) {
        printf("Core %u: tx_pps: %.0f tx_gbps: %.2f rx_pps: %.0f rx_gbps: %.2f latency: %.12f us\n",
               rte_lcore_id(), tx_pps, tx_bps/1000000000.0f,
               rx_pps, rx_bps / 1000000000.0f,
               (*delay/(double)*rx_pkts) * 1000000);
        *cnt = 0;
        *delay = 0;
        *rx_pkts = 0;
        *rx_bytes = 0;
        *tx_bytes = 0;
        *tx_pkts = 0;
        *start_time = now;
    }
}

void producer_loop(struct gen_config *config) {
    struct ether_hdr *eth_hdr;
    struct ipv4_hdr *ip_hdr;
    struct udp_hdr *udp_hdr;
    uint16_t pkt_size;
    uint64_t flow, key;


    struct rte_mbuf *buf;
    struct rte_mbuf *tx_bufs[NUM_PKTS];
    uint32_t nb_tx, tx_rate = config->tx_rate, tx_head, i;
    uint64_t tx_bytes = 0, tx_pkts = 0, cnt = 0;

    struct gen_config cfg;
    cfg.port = config->port;
    cfg.role = config->role;
    cfg.tx_rate = config->tx_rate;
    cfg.warmup = config->warmup;
    cfg.duration = config->duration;
    cfg.num_flows = config->num_flows;
    cfg.ip_min = config->ip_min;
    cfg.udp_min = config->udp_min;
    cfg.size_min = config->size_min;
    cfg.size_max = config->size_max;
    cfg.seed = config->seed;
    cfg.rx_pool = config->rx_pool;
    cfg.tx_pool = config->tx_pool;
    cfg.rx_ring_size = config->rx_ring_size;
    cfg.tx_ring_size = config->tx_ring_size;
    cfg.flags = config->flags;

    struct rte_mbuf *rx_bufs[cfg.rx_ring_size] UNUSED;
    uint32_t nb_rx;
    uint64_t rx_bytes = 0, rx_pkts = 0;
    double delay, now;

	printf("\nCore %u running.\n", rte_lcore_id());

    /* Generate NUM_PKTS worth of traffic */
    key = ranval(&cfg.seed) % cfg.num_flows;
    for (tx_head = 0; tx_head < NUM_PKTS; tx_head++) {
        tx_bufs[tx_head] = rte_pktmbuf_alloc(cfg.tx_pool);
        buf = tx_bufs[tx_head];
        pkt_size = gen_pkt_size(&cfg);
        buf->pkt_len = pkt_size - 4;
        buf->data_len = pkt_size - 4;
        buf->nb_segs = 1;
        buf->l2_len = sizeof(struct ether_hdr);
        buf->l3_len = sizeof(struct ipv4_hdr);

        eth_hdr = rte_pktmbuf_mtod(buf, struct ether_hdr *);
        ether_addr_copy(&ether_dst, &eth_hdr->d_addr);
        ether_addr_copy(&ether_src, &eth_hdr->s_addr);
        eth_hdr->ether_type = rte_cpu_to_be_16(ETHER_TYPE_IPv4);

        ip_hdr = (struct ipv4_hdr *)(eth_hdr + 1);
        memset(ip_hdr, 0, sizeof(*ip_hdr));
        ip_hdr->type_of_service = 0;
        ip_hdr->fragment_offset = 0;
        ip_hdr->time_to_live = 64;
        ip_hdr->next_proto_id = 17;
        ip_hdr->packet_id = 0;

        flow = ranval(&cfg.seed) % cfg.num_flows;
        ip_hdr->src_addr = rte_cpu_to_be_32(flow * cfg.ip_min / cfg.num_flows);
        ip_hdr->dst_addr = rte_cpu_to_be_32((flow ^ key) * cfg.ip_min / cfg.num_flows);
        ip_hdr->total_length = rte_cpu_to_be_16(pkt_size - 4 - sizeof(*eth_hdr));

        udp_hdr = (struct udp_hdr *)(ip_hdr + 1);
        udp_hdr->src_port = rte_cpu_to_be_16(ip_hdr->src_addr % cfg.udp_min);
        udp_hdr->dst_port = rte_cpu_to_be_16(ip_hdr->dst_addr % cfg.udp_min);
        udp_hdr->dgram_cksum = 0;
        udp_hdr->dgram_len = rte_cpu_to_be_16(pkt_size - 4 - sizeof(*eth_hdr) -
                                              sizeof(*ip_hdr));
        uint8_t *p = rte_pktmbuf_mtod_offset(buf, uint8_t *,
                    sizeof(struct ether_hdr) + sizeof(struct ipv4_hdr) +
                    sizeof(struct udp_hdr));
        memset(p, 0, pkt_size - 4 - sizeof(*eth_hdr) - sizeof(*ip_hdr) - sizeof(*udp_hdr) - 1); 
        if (cfg.flags & FLAG_RANDOMIZE_PAYLOAD) {
            unsigned r = 0;
            while (r < pkt_size - 4 - sizeof(*eth_hdr) - sizeof(*ip_hdr) - sizeof(*udp_hdr) - 1) {
                p[r] = (uint8_t)ranval(&cfg.seed);
                r++;
            }
        }
    }

    cfg.start_time = get_time_sec();
    tx_head = 0;
    delay = 0;

    /* Flush the RX queue */
    printf("Core %u: Flusing port %u RX queue\n", rte_lcore_id(), cfg.port);
    while (rte_eth_rx_queue_count(cfg.port, 0) > 0) {
        nb_rx = rte_eth_rx_burst(cfg.port, 0, rx_bufs, cfg.rx_ring_size);
        for (i = 0; i < nb_rx; i++) {
            rte_pktmbuf_free(rx_bufs[i]);
        }
    }

    int64_t burst;
    double start_time = get_time_sec();
    uint64_t n = 0;
    double mrxpps = 0, varrxpps = 0, mrxbps = 0, varrxbps = 0,
           mtxpps = 0, vartxpps = 0, mtxbps = 0, vartxbps = 0,
           mtxwire = 0, vartxwire = 0, mrxwire = 0, varrxwire = 0;

    while (unlikely((now = get_time_sec()) - cfg.start_time < cfg.duration)) {
        if (now - cfg.start_time > cfg.warmup) {
            stats(&tx_rate, &tx_bytes, &tx_pkts, &start_time, &cnt, &rx_bytes, &rx_pkts, &delay,
            &mrxpps , &varrxpps, &mrxbps, &varrxbps, &mrxwire, &varrxwire,
            &mtxpps , &vartxpps, &mtxbps, &vartxbps, &mtxwire, &vartxwire, &n);
        }

        uint64_t exp_bytes = (now - start_time) * tx_rate * 1000000 / 8;
        int64_t avg_pkt = (tx_bytes + 1) / (tx_pkts + 1);
        burst = (exp_bytes - tx_bytes);
        burst /= avg_pkt;
        burst = RTE_MIN(burst, (unsigned)BURST_SIZE);
        burst = RTE_MAX((unsigned)0, burst);

        nb_rx = rte_eth_rx_burst(cfg.port, 0, rx_bufs, cfg.rx_ring_size);

        for (i = 0; i < nb_rx; i++) {
            rx_bytes += rx_bufs[i]->pkt_len;
            if (cfg.flags & FLAG_MEASURE_LATENCY) {
                double *p = rte_pktmbuf_mtod_offset(rx_bufs[i], double *,
                            sizeof(struct ether_hdr) + sizeof(struct ipv4_hdr) +
                            sizeof(struct udp_hdr));
                delay += now - *p;
            }
            rte_pktmbuf_free(rx_bufs[i]);
        }

        rx_pkts += nb_rx;

        if (unlikely(tx_head + burst > NUM_PKTS)) {
            tx_head = 0;
        }

        if (cfg.flags & FLAG_MEASURE_LATENCY) {
            now = get_time_sec();
            for (i = 0; i < burst; i++) {
                double *p = rte_pktmbuf_mtod_offset(tx_bufs[tx_head + i], double *,
                            sizeof(struct ether_hdr) + sizeof(struct ipv4_hdr) +
                            sizeof(struct udp_hdr));
                *p = now;
            }
        }

        nb_tx = rte_eth_tx_burst(cfg.port, 0, tx_bufs + tx_head, burst);

        for (i = 0; i < nb_tx; i++) {
            tx_bytes += tx_bufs[tx_head + i]->pkt_len;
        }
        tx_pkts += nb_tx;

        tx_head += nb_tx;
        tx_head %= NUM_PKTS;
    }

    rte_delay_us(100);
    for (i = 0; i < NUM_PKTS; i++) {
        rte_pktmbuf_free(tx_bufs[i]);
    }

    vartxpps /= (n - 1); varrxpps /= (n - 1);
    vartxbps /= (n - 1); varrxbps /= (n - 1);
    vartxwire /= (n - 1); varrxwire /= (n - 1);

    printf("Core %u: Results\n", rte_lcore_id());
    printf("{\"tx_mpps_mean\": %9.6f,"
           "\"tx_mpps_std\": %9.6f,"
           "\"tx_mbps_mean\": %9.6f,"
           "\"tx_mbps_std\": %9.6f,"
           "\"tx_wire_mean\": %9.6f,"
           "\"tx_wire_std\": %9.6f,\n     "
           "\"rx_mpps_mean\": %9.6f,"
           "\"rx_mpps_std\": %9.6f,"
           "\"rx_mbps_mean\": %9.6f,"
           "\"rx_mbps_std\": %9.6f,"
           "\"rx_wire_mean\": %9.6f,"
           "\"rx_wire_std\": %9.6f"
           "}\n",
           mtxpps, sqrt(vartxpps), mtxbps, sqrt(vartxbps), mtxwire, sqrt(vartxwire),
           mrxpps, sqrt(varrxpps), mrxbps, sqrt(varrxbps), mrxwire, sqrt(varrxwire));
}

int launch_producer(void *config) {
    producer_loop((struct gen_config*)config);
    return 0;
}
